## This is a sample Python package to use Gitlab's Pypi server

 [This guide was followed](https://gitlab.com/help/user/packages/pypi_repository/index.md)

How to upload?
	`python3 -m twine upload --repository gitlab dist/*`

How to install?

    pip install --index-url https://__token__:<personal_access_token>@gitlab.com/api/v4/projects/20698934/packages/pypi/simple --no-deps mypypipackage

This repository also have a pipeline used to install automatically the package in the Gitlab's Pypi server